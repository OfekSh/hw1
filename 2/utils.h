#pragma once
#ifndef UTILS_H
#define UTILS_H
#include "linkedList.h"
#include "stack.h"

void reverse(int* nums, unsigned int size);
int* reverse10();

#endif // UTILS_H