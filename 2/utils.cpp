#include "utils.h"

#define SIZE 10

/*
This function reverses an array(using a stack).
*/
void reverse(int* nums, unsigned int size)
{
	stack* s = new stack;
	unsigned int i = 0;

	initStack(s);
	for (i = 0; i < size; i++) //Moving the values of the array reversed into the stack.
	{
		push(s, nums[i]);
	}

	for (i = 0; i < size; i++) //Moving back the values from the stack to the array(they are reversed in the stack).
	{
		nums[i] = pop(s);
	}
}

/*
This function creates an array, reverses the numbers inputed, 
moving them into the array, and returns it.
*/
int* reverse10()
{
	int* arr;
	int i = 0;
	int num = 0;

	stack* s = new stack;

	initStack(s);	//Initializing the stack

	for (i = 0; i < SIZE; i++)	//Recieving numbers from the user and pushing them into the stack.
	{
		
		std::cout << "Enter the " << i + 1 << " number: ";
		std::cin >> num;
		push(s, num);
	}

	arr = new int[SIZE];	//Creating the array in the size of 10.

	for (i = 0; i < SIZE; i++) //Moving the items into the array.
	{
		arr[i] = pop(s);
	}

	return arr;
}
