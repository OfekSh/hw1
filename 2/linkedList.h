#pragma once
#include <iostream>

typedef struct list	//This is the struct of the list
{
	int value;
	list* next;
}list;

list* add(list* head, int value);	//This function adds to the list, and changes the head to this item.
int remove(list** head);	//This function removes the last item in the list(which is the head).
