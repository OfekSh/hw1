#include "linkedList.h"

/*
This funcion pushes a new node into the linked list(will be the new head).
*/
list* add(list* head, int value)
{
	list* newHead = new list;

	newHead->value = value;
	newHead->next = head;

	return newHead;
}
/*
This function pops the value of the last node inputed.
*/
int remove(list** head)
{
	int value = 0;
	list* curr = (*head);

	value = (*head)->value;
	*head = (*head)->next;
	delete(curr);	//Freeing the memory of the deleted node.

	return value;
}