#include "stack.h"


void push(stack* s, unsigned int element)
{
	list* newNode = new list;

	newNode->value = element;
	newNode->next = s->line;
	s->line = newNode;
}

int pop(stack* s)
{
	int value = -1;

	if (s->line)
	{
		value = s->line->value;
		s->line = s->line->next;
	}

	return value;
}

/*
I didn't really understand this part, so I asked someone from the class
Credit for Tal
This function initializes the stack, and gives it a first node with a default value.
*/
void initStack(stack* s)
{
	s->line = new list;
	s->line = NULL;

}

void cleanStack(stack* s)
{
	list* curr = s->line;
	list* last = s->line;

	if (s->line)
	{
		curr = curr->next;
		while (curr)
		{
			delete(last);
			last = curr;
			curr = curr->next;
		}
		delete(last);
	}
}