#include "queue.h"

void initQueue(queue* q , unsigned int size)
{
	unsigned int i = 0;
	q->line = new int[size];
	q->size = size;
	for (i = 0; i < size; i++) //Initialaizing the values of the array to -1.
	{
		q->line[i] = -1;
	}
}

void cleanQueue(queue* q)
{
	int i = 0;
	

	for (i = 0; i < q->size; i++)
	{
		q->line[i] = -1;
	}
}

void enqueue(queue* q, unsigned int newValue)
{
	int index = 0;
	int *lastIndex = &index;

	if (!isFull(q, lastIndex))
	{
		q->line[index] = newValue;
	}
	else
	{
		std::cout << "queue is full.";
	}
}

int dequeue(queue* q) // return element in top of queue, or -1 if empty
{
	int numToRet = 0;

	numToRet = q->line[0];
	oneStep(q);

	return numToRet;
}

/*
This function moves the values of the array one step ahed
(Moves the value from the 'n' spot to the 'n - 1' spot).
*/
void oneStep(queue* q)
{
	int i = 0;
	

	for (i = 0; i < q->size - 1; i++)
	{
		q->line[i] = q->line[i + 1];
	}
	q->line[q->size - 1] = -1;
}

bool isFull(queue* q, int* lastIndex)
{
	int i = 0;
	bool isListFull = true;

	*lastIndex = -1;

	for (i = 0; i < q->size && isListFull; i++)
	{
		if (q->line[i] == -1)
		{
			isListFull = false;
			*lastIndex = i;
		}
	}

	return isListFull;
}