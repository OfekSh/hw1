#pragma once
#ifndef QUEUE_H
#define QUEUE_H
#include "pch.h"
#include <iostream>

/* a queue contains positive integer values. */
typedef struct queue
{
	int *line;
	int size;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

bool isFull(queue* q, int* lastIndex);
void oneStep(queue* q);

#endif /* QUEUE_H */